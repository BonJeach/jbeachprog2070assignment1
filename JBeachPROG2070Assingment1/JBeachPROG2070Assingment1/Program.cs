﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JBeachPROG2070Assingment1
{
    class Program
    {
        static void Main(string[] args) lpl 
        {
            string chosenOption;
            int circleRadius;
            Circle circle;

            Console.WriteLine("Enter A Cricle Radius Greater Than 0 (Whole Numbers Only)");
            while (true)
            {
                try
                {
                    circleRadius = int.Parse(Console.ReadLine());
                    if (circleRadius > 0) { circle = new Circle(circleRadius); break; }
                    else { Console.WriteLine("Not a valid input try again!"); }
                }
                catch
                {
                    Console.WriteLine("Not a valid input try again!");
                }
            }

            Console.Clear();
            Console.WriteLine("1. Get Circle Radius\n" +
                "2. Change Circle Radius\n" +
                "3. Get Circle Circumferenc\n" +
                "4. Get Circle Area\n" +
                "5. Exit");

            while (true)
            {
                while (true)
                {
                    chosenOption = Console.ReadLine();
                    if (chosenOption != "1" &&
                        chosenOption != "2" &&
                        chosenOption != "3" &&
                        chosenOption != "4" &&
                        chosenOption != "4")
                    {
                        Console.WriteLine("Not a valid input try again!");
                    }
                    else { break; }
                }
                switch (chosenOption)
                {
                    case "1":
                        Console.WriteLine($"Radius: {circle.GetRadius()}");
                        break;
                    case "2":
                        Console.WriteLine("Enter New Circle Radius");

                        while (true)
                        {
                            try
                            {
                                circleRadius = int.Parse(Console.ReadLine());
                                if (circleRadius > 0) { circle = new Circle(circleRadius); break; }
                                else { Console.WriteLine("Not a valid input try again!"); }
                            }
                            catch
                            {
                                Console.WriteLine("Not a valid input try again!");
                            }
                        }

                        circle.SetRadius(circleRadius);
                        Console.WriteLine("Radius Changed!");
                        break;
                    case "3":
                        Console.WriteLine($"Circumfrence: {circle.GetCircumference()}");
                        break;
                    case "4":
                        Console.WriteLine($"Area: {circle.GetArea()}");
                        break;
                    case "5":
                        Environment.Exit(0);
                        break;
                }
            }
        }
    }
}
