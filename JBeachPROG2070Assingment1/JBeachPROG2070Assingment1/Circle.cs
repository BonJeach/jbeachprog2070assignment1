﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JBeachPROG2070Assingment1
{
    class Circle
    {
        private int circleRadius;

        public Circle()
        {
            circleRadius = 1;
        }

        public Circle(int radius)
        {
            circleRadius = radius;
        }

        public int GetRadius()
        {
            return circleRadius;
        }
        public void SetRadius(int radius)
        {
            circleRadius = radius;
            return;
        }
        public double GetCircumference()
        {
            double circumference = Math.PI * 2 * circleRadius;
            circumference = Math.Round(circumference, 2);
            return circumference;
        }
        public double GetArea()
        {
            double area = Math.PI * circleRadius * circleRadius;
            area = Math.Round(area, 2);
            return area;
        }
    }
}
