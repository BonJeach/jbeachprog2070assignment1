﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace JBeachPROG2070Assingment1.Tests
{
    [TestFixture]
    public class Program
    {
        [Test]
        public void GetRadius_Given5_ResultIs5()
        {
            int sum = 5;
            Circle circle = new Circle(5);

            sum = circle.GetRadius();

            Assert.AreEqual(5, sum);
        }

        [Test]
        public void SetRadius_Given3_ResultIs3()
        {
            int sum = 6;
            Circle circle = new Circle(5);

            circle.SetRadius(3);
            sum = circle.GetRadius();

            Assert.AreEqual(3, sum);
        }

        [Test]
        public void GetCircumfrence_Given2_ResultIs12_57()
        {
            double sum = 0;
            Circle circle = new Circle(2);

            sum = circle.GetCircumference();

            Assert.AreEqual(12.57, sum);
        }

        [Test]
        public void GetArea_Given4_ResultIs50_27()
        {
            double sum = 0;
            Circle circle = new Circle(4);

            sum = circle.GetArea();

            Assert.AreEqual(50.27, sum);
        }
    }
}
